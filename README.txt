# Sync Text Attributes

Copies the text and its attributes (font, font size etc.) from one text layer to another.
It can't copy these, because they are 'read-only' for some reason: Bold, Italic, All Caps, Small Caps, Superscript, Subscript

- Copy the "syncTextAttributes.jsx" into the After Effects installation folder's "ScriptUI Panels" directory
- Restart After Effects
- Open the panel from Window>syncTextAttributes.jsx
- Create some text layers
- Link the layers:
    - Select all text layers that should inherit attributes
    - Lastly, select the 'parent' layer
    - Click the "Link"
    - This will add an expression to all childrens' "Source Text" property. This is needed by the sync functionality
- Click "Sync" to copy the parent layer's text attributes to its children
- To unlink layer, disable or remove the expression added to the "Source Text" property.


# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2017-10-21
### Added
- 'Sync' button for simpler and faster linking of layers
- 'Keep Contents' checkbox
- Checkboxes for selecting which attributes to sync

## [1.1.1] - 2017-10-19
### Changed
- Edited README.txt for better expression description

## [1.1.0] - 2017-10-19
### Added
- Single undo for all layers updated
- Checkbox: "Keep Contents" to preserve text contents