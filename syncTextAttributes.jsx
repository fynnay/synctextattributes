﻿{
function myScript(thisObj){
    function myScript_buildUI(thisObj){ //this code will be global for the script and allow it to reference the scriptUI folder        
        // USER INTERFACE
        var uiPanel = (thisObj instanceof Panel) ? thisObj : new Window("palette","Sync Text Attributes",undefined,{resizeable:true})
        //the alignment strings at the beginning of the 'res' make all sub-groups resize when the window is resized
        var res = "group{orientation:'column',spacing:5,alignment:['fill','fill'],alignChildren:['fill','fill'],\
            groupOne: Group{orientation:'row',alignment:['fill','fill'],alignChildren:['left','fill'],\
                btn_link: Button{text:'Link'},alignment:['fill','center'],preferredSize:[-1,30]\
                chB_contents: Checkbox{text:'Keep Contents',value:true,alignment:['fill','center']}\
            }\
            groupTwo: Group{orientation:'row',alignment:['fill','fill'],alignChildren:['left','fill'],\
                groupThree: Group{orientation:'column',spacing:1,alignment:['left','fill'],alignChildren:['left','fill'],\
                    chB_font:           Checkbox{text:'Font',value:true,alignment:['left','center']}\
                    chB_size:           Checkbox{text:'Font Size',value:true,alignment:['left','center']}\
                    chB_fill:           Checkbox{text:'Fill Color',value:true,alignment:['left','center']}\
                    chB_strokeColor:    Checkbox{text:'Stroke Color',value:true,alignment:['left','center']}\
                    chB_strokeWidth:    Checkbox{text:'Stroke Width',value:true,alignment:['left','center']}\
                    chB_sOverF:         Checkbox{text:'Stroke over Fill',value:true,alignment:['left','center']}\
                }\
                groupFour: Group{orientation:'column',spacing:1,alignment:['left','fill'],alignChildren:['left','fill'],\
                    chB_tsume:          Checkbox{text:'Tsume',value:true,alignment:['left','center']}\
                    chB_tracking:       Checkbox{text:'Tracking',value:true,alignment:['left','center']}\
                    chB_justification:  Checkbox{text:'Justification',value:true,alignment:['left','center']}\
                    chB_baseline:       Checkbox{text:'Baseline Shift',value:true,alignment:['left','center']}\
                    chB_verticalScale:  Checkbox{text:'Vertical Scale',value:true,alignment:['left','center']}\
                    chB_horizontalScale:Checkbox{text:'Horizontal Scale',value:true,alignment:['left','center']}\
                }\
            }\
            btn_sync: Button{text:'Sync'},alignment:['fill','top'],preferredSize:[-1,30]\
        }";
        uiPanel.grp = uiPanel.add(res);

        // ASSIGNMENTS
        // Buttons
        var btn_link = uiPanel.grp.groupOne.btn_link;
        var btn_sync = uiPanel.grp.btn_sync;
        // Checkboxes
        var chB_contents        = uiPanel.grp.groupOne.chB_contents;
        var chB_font            = uiPanel.grp.groupTwo.groupThree.chB_font;
        var chB_size            = uiPanel.grp.groupTwo.groupThree.chB_size;
        var chB_fill            = uiPanel.grp.groupTwo.groupThree.chB_fill;
        var chB_strokeColor     = uiPanel.grp.groupTwo.groupThree.chB_strokeColor;
        var chB_strokeWidth     = uiPanel.grp.groupTwo.groupThree.chB_strokeWidth;
        var chB_sOverF          = uiPanel.grp.groupTwo.groupThree.chB_sOverF;
        
        var chB_tsume           = uiPanel.grp.groupTwo.groupFour.chB_tsume;
        var chB_tracking        = uiPanel.grp.groupTwo.groupFour.chB_tracking;
        var chB_baseline        = uiPanel.grp.groupTwo.groupFour.chB_baseline;
        var chB_justification   = uiPanel.grp.groupTwo.groupFour.chB_justification;
        var chB_verticalScale   = uiPanel.grp.groupTwo.groupFour.chB_verticalScale;
        var chB_horizontalScale = uiPanel.grp.groupTwo.groupFour.chB_horizontalScale;

        //DEFAULTS
        btn_link.helpTip = "Links all selected text layers to the last selected text layer.\nYou can parent the Source Text yourself if eg. the layers are not in the same comp."
        chB_contents.helpTip = "When enabled, the original text of each child-layer is preserved. When unchecked they will inherit the text of its parent."
        btn_sync.helpTip = "Sync the selected properties of all text layers in the active comp."

        // FUNCTIONS
        function linkTextLayers(){
            /* Links all text layers to the last selected text layer */
            var comp = app.project.activeItem;
            // Cancel if selected item is not a comp
            if(!(comp instanceof CompItem)){
                alert("Please select a comp first.")
                return -1
            }
            var selLayers = comp.selectedLayers
            // Cancel if not enough text layers selected
            if(selLayers == undefined){
                alert("Please select at least 2 text layers.")
                return -1
            }
            // Get all text layers in selection
            var textLayers = [];
            for(var i=0; i<selLayers.length; i++){
                var curL = selLayers[i];
                if(curL instanceof TextLayer){
                    textLayers.push(curL)
                }
            }
            // Cancel if not enough text layers selected
            if(!(textLayers.length > 1)){
                alert("Please select at least 2 text layers.")
                return -1
            }
            // Link text layers
            app.beginUndoGroup("Link text layers")
            var parL = textLayers[textLayers.length-1];
            for(var i=0; i<textLayers.length-1; i++){
                var curL = textLayers[i];
                if(curL.property("Source Text").canSetExpression){
                    var expression = 'thisComp.layer("'+parL.name+'").text.sourceText;';
                    if(chB_contents.value){
                        expression+= "\nvalue;"
                    }
                    curL.property("Source Text").expression = expression;
                }
            }
            app.endUndoGroup()
            // Sync font attributes immideately
            //syncFontAttributes();
            return 1
        };

        function syncFontAttributes(){
            /* Syncs all child layers to their linked parent */
            var comp = app.project.activeItem;
            // Cancel if selected item is not a comp
            if(!(comp instanceof CompItem)){
                alert("Please select a comp first.")
                return -1
            }
            // Find all text layers
            var allTextLayers = [];
            for(i=0;i<comp.numLayers;i++){
                var curL = comp.layer(i+1) // after effects' layer index starts at 1
                if(curL instanceof TextLayer){
                    allTextLayers.push(curL)
                }
            }
            // Sync font attributes
            app.beginUndoGroup("Sync Text Attributes")
            for(var i=0; i<allTextLayers.length; i++){
                var curL = allTextLayers[i];
                var curLTxt = curL.property("Source Text")
                var parL = null;
                // Find parent layer defined in expression
                if (curLTxt.expressionEnabled == true){
                    var expr = curLTxt.expression
                    var regex = /\.layer\("(.+)"\)\.text\.sourceText/g;
                    if ((m = regex.exec(expr)) !== null) {
                        // Skip item if parent layer can't be found
                        if(m[1] == null){
                            continue;
                        }
                        // Assign parentLayer
                        parL = comp.layer(m[1])
                    }
                }
                // Skip if parent is null
                if(parL == null){
                    // $.writeln("Could not find parent layer's name. Skipping item '"+curL.name+"'.")
                    continue;
                }
                // Skip if parent is not a text object
                if(!(parL instanceof TextLayer)){
                    // $.writeln("Parent defined is not a text layer. Skipping item '"+curL.name+"'.")
                    continue;
                }
                // Set value from parent
                var parTxt = parL.property("Source Text").value;
                // temporarily disable expression to get actual value
                curL.property("Source Text").expressionEnabled = false;
                var curTxt = curL.property("Source Text").value.text;
                var newTxt = new TextDocument(curTxt);
                // Assign text document to text layer to be able to edit its properties
                curLTxt.setValue(newTxt)
                var txtDoc = curLTxt.value
                // --Text
                txtDoc.text = curTxt
                // reenable expression
                allTextLayers[i].property("Source Text").expressionEnabled = true;
                // --Font
                if(chB_font.value){
                    txtDoc.font = parTxt.font;
                    // Don't work. Also, they are synced by .font already
                    // txtDoc.fontFamily     = parTxt.fontFamily;
                    //txtDoc.fontStyle      = parTxt.fontStyle;
                }
                if(chB_size.value){
                    txtDoc.fontSize = parTxt.fontSize;
                }
                // --Color
                if(chB_fill.value){
                    txtDoc.applyFill = parTxt.applyFill;
                    if(parTxt.applyFill == true){
                        txtDoc.fillColor      = parTxt.fillColor;
                    }
                }
                if(chB_strokeColor.value || chB_strokeWidth.value){
                    txtDoc.applyStroke    = parTxt.applyStroke;
                    if(parTxt.applyStroke == true){
                        if(chB_strokeColor.value){
                            txtDoc.strokeColor = parTxt.strokeColor;
                        }
                        if(chB_strokeWidth.value){
                            txtDoc.strokeWidth = parTxt.strokeWidth;
                        }
                    }
                }
                // BUG if strokeOverFill is set to "All Fills over all strokes" or "All strokes over all fills" it always returns false
                if(chB_sOverF.value){
                    txtDoc.strokeOverFill = parTxt.strokeOverFill;
                }
                // --Position+Scale
                if(chB_tsume.value){
                    txtDoc.tsume = parTxt.tsume
                }
                if(chB_tracking.value){
                    txtDoc.tracking = parTxt.tracking;
                }
                if(chB_baseline.value){
                    txtDoc.baselineShift = parTxt.baselineShift
                }
                if(chB_justification.value){
                    txtDoc.justification = parTxt.justification;
                }
                if(chB_verticalScale.value){
                    txtDoc.verticalScale = parTxt.verticalScale
                }
                if(chB_horizontalScale.value){
                    txtDoc.horizontalScale = parTxt.horizontalScale
                }
                // --Probably never would want to sync these. Probably. But keeping them here in case future requests and stuff.
                //txtDoc.pointText      = parTxt.pointText;
                //txtDoc.boxText        = parTxt.boxText;
                // if(parTxt.boxText == true){
                //     txtDoc.boxTextSize    = parTxt.boxTextSize;
                // }
                // --These are read only
                // fauxBold
                // fauxItalic
                // smallCaps
                // subscript
                // superscript
                curLTxt.setValue(txtDoc)
                //curL.property("Source Text").setValue(parL.property("Source Text").value)
            }
            app.endUndoGroup()
        };
        
        // SIGNALS
        btn_link.onClick = linkTextLayers;
        btn_sync.onClick = syncFontAttributes;
        
        // SET UP PANEL SIZING
        uiPanel.layout.layout(true); //this reformats the entire layout
        uiPanel.grp.minimumSize = uiPanel.grp.size;  //this clamps the window size so you can't make it smaller than the size of all elements combined

        // MAKE THE PANEL RESIZEABLE
        uiPanel.layout.resize();
        uiPanel.onResizing = uiPanel.onResize = function(){this.layout.resize()} //this centers the content (main group) inside the window, no matter the size

        return uiPanel;
    }
    var myScriptPal = myScript_buildUI(thisObj);

    if((myScriptPal!=null)&&(myScriptPal instanceof Window)){
        //myScriptPal.center();
        myScriptPal.show();
    }
}
myScript(this); //"this" references the function that opens the panel/window
}